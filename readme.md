# Работа с Linux

 - [Системные команды](system/readme.md)
 - [Web-сервер Nginx](nginx/readme.md)
 - [Установка пакетов](packages/readme.md)
