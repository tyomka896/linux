Обновить индексы локальных пакетов

```bash
sudo apt update
```

Загрузить скрипт, например, для версии 18

```bash
curl -sL https://deb.nodesource.com/setup_18.x -o /tmp/nodesource_setup.sh
```

Запустить скрипт, который обновит локальный кэш зависимостей

```bash
sudo bash /tmp/nodesource_setup.sh
```

Установить NodeJS

```bash
sudo apt install nodejs -y
```

И проверить версию установленного пакета

```bash
node -v
# Output: v18.*

npm -v
# Output: v8.*
```

> Для большей информации можно обратиться к репозиторию о документации по установке и поддержки NodeJS [nodesource/distributions](https://github.com/nodesource/distributions)
