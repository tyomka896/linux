# Создание не докер версии

- Getting started: https://kafka.apache.org/quickstart
- Install via systemctl: https://thelinuxnotes.com/index.php/how-to-install-and-configure-apache-kafka-web-ui-docker/
- About systemctl: https://www.golinuxcloud.com/create-systemd-service-example/ and https://www.tecmint.com/create-systemd-service-linux/

В файле `config/server.properties` обязательно раскоментировать и установить параметр

```bash
advertised.listeners=PLAINTEXT://{local_ip_address}:9092
```

А `listeners` можно оставить не заполненным

## Запуск через Nohup

Для простого примера можно использовать `nohup`:

```
nohup bash -c 'kafka/bin/zookeeper-server-start.sh kafka/config/zookeeper.properties' > zookeeper.log

nohup bash -c 'kafka/bin/kafka-server-start.sh kafka/config/server.properties' > kafka.log
```

Чтобы найти процесс от `nohup` и остановить его можно воспользоваться:

```
lsof kafka.log

kill {pid}
```

## Написание .service файла

Пример создания сервиса для zookeeper:

```
[Unit]
Description=Zookeeper
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
ExecStart={path}/kafka/bin/zookeeper-server-start.sh \
    {path}/kafka/config/zookeeper.properties
ExecStop={path}/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

Пример создания сервиса для kafka:

```
[Unit]
Description=Kafka
Requires=zookeeper.service
After=zookeeper.service

[Service]
ExecStart={path}/kafka/bin/kafka-server-start.sh \
    {path}/kafka/config/server.properties
ExecStop={path}/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

Запуск через

```
systemctl start zookeeper
systemctl start kafka
```