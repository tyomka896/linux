# Веб менеджер Kafka UI

Основной репозиторий: https://github.com/provectus/kafka-ui

Пример `docker-compose.yml` для разворачивания веб приложения

```
version: '3'

services:
  kafka-ui:
    container_name: kafka-ui
    image: provectuslabs/kafka-ui:latest
    ports:
      - 8080:8080
    environment:
      DYNAMIC_CONFIG_ENABLED: true
      KAFKA_CLUSTERS_0_NAME: local
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: host.docker.internal:9092
      KAFKA_CLUSTERS_0_ZOOKEEPER: host.docker.internal:2181
    extra_hosts:
      - host.docker.internal:host-gateway
```