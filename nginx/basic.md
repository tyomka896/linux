[« Назад](readme.md)

# Установка Nginx

Установить _nginx_ дистрибутив

```bash
# Debian
apt install nginx -y
```

Создать директорию приложения

```bash
mkdir -p /var/www/example.ru/html
```

Создать простой _HTML-документ_

```bash
nano /var/www/example.ru/html/index.html
```

Пример страницы

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web App</title>
</head>
<body>
    <h2>Hello, Web App!</h2>
</body>
</html>
```

Создать конфигурацию в обшей директории всех приложений

```bash
nano /etc/nginx/sites-available/example.ru
```

С содержимым

> Атрибут `default_server` используется для обозначения сервера по умолчанию и его можно опустить, если сервер всего один

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name example.ru www.example.ru;

    root /var/www/example.ru/html;
    index index.html index.xml;
}
```

Создать ссылку в директорию активных приложений

```bash
ln -s /etc/nginx/sites-available/example.com.conf /etc/nginx/sites-enabled/
```

Проверить конфигурацию _nginx_

```bash
nginx -t
```

Успешная настройка будет гарантирована

```
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Перезапустить сервер

```bash
systemctl restart nginx
```

Проверить работу сервера запросом, результатом на который будет созданный _HTML-документ_

```bash
curl -X GET localhost
# или
curl -X GET http://example.ru
```
