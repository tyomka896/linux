[« Назад](readme.md)

# Сертификат для Nginx

> ВНИМАНИЕ! Сертификат можно установить только для уже работающего Веб-приложения по существующему доменному имени, подробнее в [Первичном запуске](basic.md).

Установить и запустить _snapd_

```bash
# Debian
apt install snapd -y
systemctl start snapd
```

Сгенерировать OpenSSL ключ

```bash
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
```

Создать папку для снипетов

```bash
mkdir -p /etc/nginx/snippets
```

Создать снипет для SSL

```bash
nano /etc/nginx/snippets/ssl-params.conf
```

Записать конфигурацию от _Mozilla_

```
ssl_session_timeout 1d;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;

ssl_dhparam /etc/ssl/certs/dhparam.pem;

ssl_protocols TLSv1.2 TLSv1.3;
ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
ssl_prefer_server_ciphers off;

add_header Strict-Transport-Security "max-age=63072000" always;
```

Обновить _snapd_

```bash
snap install core
```

Установить _certbot_

```bash
snap install --classic certbot
```

Создать ссылку _certbot_ для использования глобально

```bash
ln -s /snap/bin/certbot /usr/bin/certbot
```

Выпустить сертификат через _certbot_ для _nginx_

```bash
certbot certonly --nginx
```

Ввести email-адрес, согласиться со всеми условиями дважды (по усмотрению) и выбрать из списка предложенных доменных имен необходимые адреса. Например, для списка через пробел перечислить оба `1 2`

```
Which names would you like to activate HTTPS for?
We recommend selecting either all domains, or all domains in a VirtualHost/server block.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: example.ru
2: www.example.ru
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel): 1 2
```

Будут сгенерированные два сертификата, убедиться в этом можно по сообщениям

```
Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/example.ru/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/example.ru/privkey.pem
This certificate expires on 2022-01-01.
```

Открыть конфигурацию приложения

```bash
nano /etc/nginx/sites-available/example.ru
```

Изменить его на

```
server {
    listen 80;
    listen [::]:80;

    server_name example.ru www.example.ru;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    server_name example.ru;
    root /var/www/example.ru/html;
    index index.html index.xml;

    ssl_certificate /etc/letsencrypt/live/example.ru/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.ru/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/example.ru/chain.pem;

    include snippets/ssl-params.conf;
}
```

Сохранить и проверить конфигурацию nginx

```bash
nginx -t
```

Перезапустить nginx

```bash
systemctl restart nginx
```

Проверить домен

```
curl -X GET http://example.ru
# Output: 301 Moved Permanently

curl -X GET https://example.ru
# Output: 200 OK
```

Для обновления списка доменов сертификата выполнить

```bash
certbot certonly --nginx --cert-name example.ru -d example.ru -d www.example.ru -d sub.example.ru -d www.sub.example.ru
```
