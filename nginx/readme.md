[« Назад](../readme.md)

# Web-сервер Nginx

 - [Первичный запуск](basic.md)
 - [Установка PHP](php.md)
 - [Добавление SSL сертификата](ssl.md)
