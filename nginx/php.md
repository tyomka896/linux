[« Назад](readme.md)

# Настройка PHP для Nginx

Установить _PHP_

```bash
# Debian
apt install php php-fpm -y
```

Проверить версию

```bash
php --version
```

Изменить конфигурацию сервера

```bash
nano /etc/nginx/sites-available/example.ru
```

> Обратить внимание на атрибут `fastcgi_pass`, в котором указывается версия установленного _PHP_

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name aesfu.ru www.aesfu.ru;
    root /var/www/aesu.ru/html/public;
    index index.php index.html;

    location / {
        try_files $uri $uri /index.php?$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    }
}
```

Переименовать _index.html_ в _index.php_

```bash
mv /var/www/example.ru/html/index.html /var/www/example.ru/html/index.php
```

Пример _PHP-страницы_ с обработкой параметра в запросе

```html
<?php
    $name = 'Web App';
    if (isset($_GET['name'])) $name = $_GET['name'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web App</title>
</head>
<body>
    <h2>Hello, <?php echo $name; ?>!</h2>
</body>
</html>
```

Проверить работу _PHP_ по запросу с параметром

```bash
curl -X GET localhost?name=User
```
