[« Назад](info.md)

# Добавление пользователя

Создать пользователя можно командой `useradd` с указанием дополнительных параметров и логина в конце

> Рекомендуется в качестве имени использовать латинские прописные буквы длинной не более восьми знаков.

```bash
useradd -с 'Main User' -m -s /bin/bash user
```

В команде указывается
 - _-с_ - комментарий
 - _-m_ - создание домашней директории
 - _-r_ - указание пути домашней директории
 - _-s_ - указание командной строки
 - _-g_ - группа по умолчанию
 - _-G_ - дополнительный список групп
 - _-r_ - создать системного пользователя
 - _-u_ - указать UID

Команда без указания параметров создаст только запись о пользователе. После выполнения команды новый пользователь появится в системе

Убедиться в создании пользователя

```bash
tail -1 /etc/passwd
```

Задать пароль созданному пользователю можно через команду `passwd` с указанием логина

```bash
passwd user
```

Результатом выполнения команды будет

```
New password:
Retype new password:
passwd: password updated successfully
```

Удалить пользователя вместе с домашней директорией

```bash
userdel -r user
```
