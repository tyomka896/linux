[« Назад](info.md)

# Управление группами

### Создание группы (groupadd)

Создать группу можно командой `groupadd` с указанием названия

```bash
groupadd -g 1100 group
```

Команда создаст группу с именем _group_ и с GroupID _1100_.

### Изменение группы (groupmod)

Изменить группу можно командой `groupmod`.

Например, переименовать группу

```bash
groupmod -n my_group group
```

Или изменить GroupID группы

```bash
groupmod -g 1200 group
```

### Удаление группы (groupdel)

Удалить группу можно командой `groupdel`

```bash
groupdel group
```
