[« Назад](info.md)

# Создание пользователя

Добавить пользователя можно командой `adduser` с указанием логина.

> Рекомендуется в качестве имени использовать латинские прописные буквы длинной не более восьми знаков.

```bash
adduser user
```

Команда по умолчанию создаст пользователя, группу с введенным логином, домашнюю директорию, дополнительно попросит задать пароль и другие комментарии

Далее необходимо будет заполнить опциональные поля и подтвердить ввод

```
Adding user `user' ...
Adding new group `user' (1004) ...
Adding new user `user' (1002) with group `user' ...
Creating home directory `/home/user' ...
Copying files from `/etc/skel' ...
New password:
Retype new password:
passwd: password updated successfully
Changing the user information for user
Enter the new value, or press ENTER for the default
        Full Name []: User
        Room Number []:
        Work Phone []:
        Home Phone []:
        Other []:
Is the information correct? [Y/n] Y
```

Убедиться в создании пользователя

```bash
tail -1 /etc/passwd
```

Удалить пользователя вместе с домашней директорией

```bash
deluser --remove-home user
```

Результатом будет

```
Looking for files to backup/remove ...
Removing files ...
Removing user `user' ...
Warning: group `user' has no more members.
```
