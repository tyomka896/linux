[« Назад](../readme.md)

# Отображение пользователей

### Основные команды

 - [Добавить пользователя](useradd.md)
 - [Создать пользователя](adduser.md)
 - [Изменение пользователя](usermod.md)
 - [Управление группами](groups.md)

### Дополнительно

Все пользователеи хранятся в файле `/etc/passwd`, которые хранятся в формате

```
username:password:UserID:GroupID:comments:home_dir:shell
```

Например

```bash
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
```

Все группы хранятся в файле `/etc/group`, которые хранятся в формате

```
group_name:password:GroupID:accountN. . .
```

Например

```bash
root:x:0:
daemon:x:1:
bin:x:2:
user:x:1000:group_one,group_two
```

Зашифрованные пароли пользователей хранятся в файле `/etc/shadow`.

Копируемые файлы по умолчанию в домашнюю директории находятся в `/etc/skel`.




